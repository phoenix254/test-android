##SPA Rent Demo
__Rent Android application__ consumes from the a dummy api on [__http://rent.jswiftdev.com__](http://rent.jswiftdev.com/api/)  
The Application is meant to be a template for a complete application with points in which one can add
custom components to make it a full application.

Download apk here [App](apks/release/spa-rent-demo.apk?raw)

![ScreenShot](screenshots/screen_1.png)

The application provides has support for both __hdpi, mdpi and ldpi__ screens.
This can be used to further support more screens.


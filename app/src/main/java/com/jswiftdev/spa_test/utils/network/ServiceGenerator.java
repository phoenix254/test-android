package com.jswiftdev.spa_test.utils.network;

import android.util.Log;

import com.jswiftdev.spa_test.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by james on 01/10/16.
 */

public class ServiceGenerator {
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        Log.i(Constants.TAG, Constants.BASE_URL_API);
        return retrofit;
    }
}

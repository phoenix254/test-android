package com.jswiftdev.spa_test.utils.tasks;

import retrofit2.Response;

/**
 * Created by james on 01/10/2017.
 */

public interface TaskProgressListener {
    void onTaskStarted();
    void onTaskComplete();
    void onTaskSuccessful(Response response);
    void onTaskFailed(String error);
}

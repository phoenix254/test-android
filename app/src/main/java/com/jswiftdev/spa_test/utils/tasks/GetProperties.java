package com.jswiftdev.spa_test.utils.tasks;

import android.os.AsyncTask;

import com.jswiftdev.spa_test.utils.network.Api;
import com.jswiftdev.spa_test.utils.network.ServiceGenerator;

import retrofit2.Response;

/**
 * Created by james on 01/10/2017.
 */

public class GetProperties extends AsyncTask<String, String, Response> {
    private static GetProperties task = null;
    private TaskProgressListener taskProgressListener;

    public static GetProperties getInstance(TaskProgressListener taskProgressListener) {
        if (task == null) {
            task = new GetProperties(taskProgressListener);
        }
        return task;
    }

    private GetProperties(TaskProgressListener taskProgressListener) {
        this.taskProgressListener = taskProgressListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        taskProgressListener.onTaskStarted();
    }

    @Override
    protected Response doInBackground(String... strings) {
        try {
            return ServiceGenerator.getClient().create(Api.class).getMostSearched().execute();
        } catch (Exception e) {
            publishProgress(e.getMessage());

            return null;
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        taskProgressListener.onTaskFailed(values[0]);
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        taskProgressListener.onTaskComplete();

        if (response != null) {
            if (response.isSuccessful()) {
                taskProgressListener.onTaskSuccessful(response);
            } else {
                taskProgressListener.onTaskFailed(response.code()+": "+response.message());
            }
        }


        task = null;
        taskProgressListener = null;
    }
}

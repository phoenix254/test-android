package com.jswiftdev.spa_test.utils.network;

import com.jswiftdev.spa_test.models.Property;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by james on 01/10/16.
 *
 * @author james
 */

public interface Api {
    @GET("properties/searched")
    Call<List<Property>> getMostSearched();

}

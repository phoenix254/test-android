package com.jswiftdev.spa_test.models;

import com.jswiftdev.spa_test.Constants;

public class Property {
    private String title;
    private String place;
    private double cost;
    private int rated;
    private String ref_number;
    private String image_url;


    public String getTitle() {
        return title;
    }

    public String getPlace() {
        return place;
    }

    public String getCost() {
        return "KES. " + String.valueOf(cost);
    }

    public int getRated() {
        return rated;
    }

    public String getRef_number() {
        return ref_number;
    }

    public String getImage_url() {
        return Constants.BASE_URL +image_url;
    }

    @Override
    public String toString() {
        return "Property{" +
                "title='" + title + '\'' +
                ", place='" + place + '\'' +
                ", cost=" + cost +
                ", rated=" + rated +
                ", ref_number='" + ref_number + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}

package com.jswiftdev.spa_test;

/**
 * Created by james on 01/10/2017.
 */

public class Constants {
//    public static String BASE_URL = "http://192.168.43.252/spa_test/";
    public static String BASE_URL = "http://rent.jswiftdev.com/";
    public static String BASE_URL_API = BASE_URL + "api/";

    public static String TAG = "riven";
}

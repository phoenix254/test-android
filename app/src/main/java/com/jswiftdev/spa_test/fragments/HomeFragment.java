package com.jswiftdev.spa_test.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jswiftdev.spa_test.MainActivity;
import com.jswiftdev.spa_test.R;
import com.jswiftdev.spa_test.models.Property;
import com.jswiftdev.spa_test.utils.SeekBarWithIntervals;
import com.jswiftdev.spa_test.utils.tasks.GetProperties;
import com.jswiftdev.spa_test.utils.tasks.TaskProgressListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by james on 28/09/2017.
 */

public class HomeFragment extends Fragment {

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    private RecyclerView rvLatestProperties;
    private RecyclerView rvMostSearchedProperties;
    private SeekBarWithIntervals skCostFilter;
    private List<Property> properties;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvLatestProperties = view.findViewById(R.id.rv_latest_properties);
        rvMostSearchedProperties = view.findViewById(R.id.rv_most_searched);
        skCostFilter = view.findViewById(R.id.sk_cost_filter);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");


        properties = new ArrayList<>();


        rvLatestProperties.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvLatestProperties.setAdapter(new PropertyAdapter(properties, PropertyAdapter.VIEWTYPE_LATEST));

        rvMostSearchedProperties.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvMostSearchedProperties.setAdapter(new PropertyAdapter(properties, PropertyAdapter.VIEWTYPE_SEARCHED));

        skCostFilter.setIntervals(new ArrayList<String>() {{
            add("10k");
            add("20k");
            add("50k");
            add("80k");
            add("100k");
            add(">100k");
        }});

        if (properties.isEmpty())
            getProperties();
    }

    private List<Property> getProperties() {
        if (properties.isEmpty()) {
            GetProperties.getInstance(new TaskProgressListener() {
                @Override
                public void onTaskStarted() {
                    progressDialog.show();
                }

                @Override
                public void onTaskComplete() {
                    progressDialog.dismiss();
                }

                @Override
                public void onTaskSuccessful(Response response) {
                    properties.addAll((List<Property>) response.body());

                    rvLatestProperties.setAdapter(new PropertyAdapter(properties, PropertyAdapter.VIEWTYPE_LATEST));
                    rvMostSearchedProperties.setAdapter(new PropertyAdapter(properties, PropertyAdapter.VIEWTYPE_SEARCHED));
                }

                @Override
                public void onTaskFailed(String error) {
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }).execute();
        }
        return properties;
    }


    class PropertyAdapter extends RecyclerView.Adapter<PropertyAdapter.PropertyViewHolder> {
        private List<Property> property;
        private int viewType;
        static final int VIEWTYPE_LATEST = 1;
        static final int VIEWTYPE_SEARCHED = 2;

        PropertyAdapter(List<Property> property, int viewType) {
            this.property = property;
            this.viewType = viewType;
        }

        @Override
        public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int templateId = viewType == VIEWTYPE_LATEST ? R.layout.single_property_latest : R.layout.single_property_searched;

            View view = LayoutInflater.from(getContext()).inflate(templateId, parent, false);
            return new PropertyViewHolder(view);
        }

        @Override
        public int getItemViewType(int position) {
            return this.viewType;
        }

        @Override
        public void onBindViewHolder(PropertyViewHolder holder, int position) {
            final Property currentProperty = property.get(position);
            holder.propertyCost.setText(currentProperty.getCost());
            holder.propertyTitle.setText(currentProperty.getTitle());

            Glide.with(getActivity())
                    .load(currentProperty.getImage_url())
                    .into(holder.propertyImage);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) getActivity()).showDetails();
                    MainActivity.activeProperty = currentProperty;
                }
            });
        }

        @Override
        public int getItemCount() {
            return property.size();
        }

        class PropertyViewHolder extends RecyclerView.ViewHolder {
            TextView propertyTitle;
            TextView propertyCost;
            ImageView propertyImage;

            PropertyViewHolder(View itemView) {
                super(itemView);
                propertyTitle = itemView.findViewById(R.id.tv_property_title);
                propertyCost = itemView.findViewById(R.id.tv_property_cost);
                propertyImage = itemView.findViewById(R.id.im_property_image);
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}

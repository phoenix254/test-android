package com.jswiftdev.spa_test.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jswiftdev.spa_test.MainActivity;
import com.jswiftdev.spa_test.R;

/**
 * Created by james on 28/09/2017.
 */

public class DetailFragment extends Fragment {
    public static DetailFragment newInstance() {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_housedetail, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (MainActivity.activeProperty != null) {
            ImageView currentImage = view.findViewById(R.id.detail_image_view);
            Glide.with(getActivity()).load(MainActivity.activeProperty.getImage_url()).into(currentImage);

            ((TextView) view.findViewById(R.id.tv_ref_number)).setText(MainActivity.activeProperty.getRef_number());
        }
    }
}
